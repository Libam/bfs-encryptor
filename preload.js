const { ipcRenderer } = require('electron');

async function init() {
    const dropArea = document.querySelector('.dropper');

    dropArea.addEventListener('dragover', (e) => {
        e.preventDefault();
        e.stopPropagation();
    });

    dropArea.addEventListener('drop', (event) => {
        event.preventDefault();
        event.stopPropagation();

        const file = event.dataTransfer.files[0].path

        if (getFileExtension(file) == "inp") {
            ipcRenderer.send('encrypt-file', file);
        }
        else {
            alert(`.${getFileExtension(file)} Dateien werden nicht unterstützt`)
        }

        ipcRenderer.on("encrypt-success", (event, result) => {
            console.log(result);
            displaySuccess(result)
        })

        ipcRenderer.on("encrypt-error", (event, result) => {
            console.log(result);
            displayError(result);
        })
    });

    ipcRenderer.on("encrypt-success", (e) => {
        console.log(e);
    })
}

function displaySuccess(result) {
    document.querySelector(".result").innerHTML = `
    <div class="success">
        <h3>Verschlüsselung erfolgreich</h3>
        <p>Datei abgelegt unter: ${result.outputFilePath}</p>
    </div>
    `
}

function displayError(result) {
    document.querySelector(".result").innerHTML = `
    <div class="error">
        <h1>Verschlüsselung fehlgeschlagen</h1>
        <p>${result.message}</p>
    </div>
    `
}

function getFileExtension(filePath) {
    const lastDotIndex = filePath.lastIndexOf('.');

    if (lastDotIndex !== -1 && lastDotIndex > 0) {
        return filePath.slice(lastDotIndex + 1);
    } else {
        return "No extension";
    }
}

document.addEventListener("DOMContentLoaded", () => init())