const { app, BrowserWindow, Menu, ipcMain, dialog } = require('electron')
const path = require('node:path')
const encryptor = require("./src/encryptor")
const fs = require("fs")
const isDev = require("electron-is-dev")


function createWindow() {
  const win = new BrowserWindow({
    width: 500,
    height: 700,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  // Create an empty menu
  const menu = Menu.buildFromTemplate([]);
  win.setMenu(menu);

  win.loadFile('./ui/index.html')

  if (isDev) {
    win.webContents.openDevTools();
  }

  ipcMain.on("encrypt-file", async (event, path) => {
    try {
      const result = await encryptor.encrypt(path)
        
      if (result.success == true) {
        const userCall = await dialog.showSaveDialog(win, {
          title: "Save encrypted file",
          defaultPath: result.outputFilePath,
          filters: [{ name: 'Encrypted Files', extensions: ['dat'] }]
        })

        if (!userCall.canceled) {
          result.outputFilePath = userCall.filePath

          fs.copyFileSync(result.tempFile, userCall.filePath)
          event.reply("encrypt-success", result)
        }
      }
      else {
        event.reply("encrypt-error", result)
      }
    }
    catch(err) {
      event.reply("encrypt-error", {success: false, message: err})
    }

    encryptor.removeTempFiles()
  })
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
