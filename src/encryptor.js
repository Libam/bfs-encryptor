const path = require('node:path')
const cp = require("node:child_process")
const fs = require('node:fs');
const isDev = require("electron-is-dev")

const executable = getExec()
const inputTempFileName = 'input.inp'

async function encrypt(originalFilePath) {
    //remove any old files
    removeTempFiles()

    //define paths
    const inputTempFile = `${path.dirname(executable)}/${inputTempFileName}`
    const outputTempFile = `${path.dirname(executable)}/${inputTempFileName.replace(".inp", ".dat")}`

    //create tempfile
    fs.copyFileSync(originalFilePath, inputTempFile)

    //encrypt
    const result = await runEncryptor()

    if (result.code == 0) {
        //copy output to original location
        const { dir, name } = path.parse(originalFilePath);
        const outputFilePath = path.join(dir, `${name}.dat`)
        
        return {
            success: true,
            tempFile: outputTempFile,
            outputFilePath: outputFilePath
        }
    }
    else {
        return {
            success: false,
            message: result.output
        }
    }
}

function runEncryptor() {
    return new Promise(resolve => {
        const encryptionProcess = cp.spawn(executable, { cwd: path.dirname(executable) })
        let output = ''

        encryptionProcess.stdout.on("data", data => output += String(data))

        encryptionProcess.on("close", code => {
            resolve({
                code: code,
                output: output
            })
        })

        encryptionProcess.stdin.write(inputTempFileName);
        encryptionProcess.stdin.end();
    })
}

function getExec() {
    if (isDev) {
        return path.join(process.cwd(), './assets/encryption/spitald.exe')
    }
    else {
        return path.join(process.cwd(), './resources/assets/encryption/spitald.exe')

    }
}

function removeTempFiles() {
    const dir = path.dirname(executable)

    for (const file of fs.readdirSync(dir)) {
        if (path.extname(file) == ".dat" || path.extname(file) == '.inp') {
            fs.unlinkSync(path.join(dir, file))
        }
    }
}

module.exports = {
    encrypt: encrypt,
    removeTempFiles: removeTempFiles
}